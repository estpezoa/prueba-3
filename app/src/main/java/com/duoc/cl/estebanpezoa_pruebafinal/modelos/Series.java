package com.duoc.cl.estebanpezoa_pruebafinal.modelos;

import com.google.gson.annotations.SerializedName;

/**
 * Created by hardroidlabs on 18-05-17.
 */

public class Series {
    @SerializedName("_id")
    private String id;
    private String title;
    private String year;
    private String[] genres;
    @SerializedName("images")
    private ImagesSerie poster;
    @SerializedName("rating")
    private RatingSerie ratingSerie;
    @SerializedName("num_seasons")
    private int numSeasons;


    public Series() {
    }

    public Series(String id, String title, String year, String[] genres, ImagesSerie poster, RatingSerie ratingSerie, int numSeasons) {
        this.id = id;
        this.title = title;
        this.year = year;
        this.genres = genres;
        this.poster = poster;
        this.ratingSerie = ratingSerie;
        this.numSeasons = numSeasons;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String[] getGenres() {
        return genres;
    }

    public void setGenres(String[] genres) {
        this.genres = genres;
    }

    public ImagesSerie getPoster() {
        return poster;
    }

    public void setPoster(ImagesSerie poster) {
        this.poster = poster;
    }

    public RatingSerie getRatingSerie() {
        return ratingSerie;
    }

    public void setRatingSerie(RatingSerie ratingSerie) {
        this.ratingSerie = ratingSerie;
    }

    public int getNumSeasons() {
        return numSeasons;
    }

    public void setNumSeasons(int numSeasons) {
        this.numSeasons = numSeasons;
    }



}
