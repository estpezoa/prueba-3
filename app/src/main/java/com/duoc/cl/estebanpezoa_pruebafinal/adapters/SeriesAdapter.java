package com.duoc.cl.estebanpezoa_pruebafinal.adapters;


import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.duoc.cl.estebanpezoa_pruebafinal.R;
import com.duoc.cl.estebanpezoa_pruebafinal.modelos.Series;
import com.squareup.picasso.Picasso;

import java.util.List;

public class SeriesAdapter extends RecyclerView.Adapter<SeriesAdapter.SeriesViewHolder> {

    private Context context;
    private List<Series> series;

    public static class SeriesViewHolder extends RecyclerView.ViewHolder {

        public CardView mCardView;
        public TextView tvAnio, tvNombre, tvGenero, tvNroTemporadas, tvRating;
        public ImageView ivPoster;




        public SeriesViewHolder(View itemView) {
            super(itemView);

            mCardView = (CardView) itemView.findViewById(R.id.card_view);
            tvAnio = (TextView) itemView.findViewById(R.id.tvAnio);
            tvNombre = (TextView) itemView.findViewById(R.id.tvNombre);
            tvGenero = (TextView) itemView.findViewById(R.id.tvGenero);
            tvNroTemporadas = (TextView) itemView.findViewById(R.id.tvNroTemporadas);
            tvRating = (TextView) itemView.findViewById(R.id.tvRating);
            ivPoster = (ImageView) itemView.findViewById(R.id.ivPoster);
        }




    }

    public SeriesAdapter(List<Series> series) {
        this.series = series;
    }

    @Override
    public SeriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_item, parent, false);

        SeriesViewHolder vh = new SeriesViewHolder(v);
        return vh;
    }


    @Override
    public void onBindViewHolder(SeriesViewHolder holder, final int position) {
        Series vet = series.get(position);
        holder.tvAnio.setText(String.valueOf(vet.getYear()));
        holder.tvNombre.setText(String.valueOf(vet.getTitle()));
        holder.tvGenero.setText("");
        for (int i = 0; i < vet.getGenres().length - 1; i++) {
            holder.tvGenero.setText(String.valueOf(holder.tvGenero.getText().toString() + "-" + String.valueOf(vet.getGenres()[i])));
        }

        holder.tvNroTemporadas.setText(String.valueOf(vet.getNumSeasons()));
        //holder.tvRating.setText((CharSequence) vet.getRatingSerie());
        Picasso.with(holder.ivPoster.getContext())
                .load(vet.getPoster().getBanner())
                .into(holder.ivPoster);

        holder.tvRating.setText(String.valueOf(vet.getRatingSerie().getPercentage()));

        holder.mCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Series currentValue = series.get(position);
                Log.d("CardView", "CardView Clicked: " + currentValue);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (series == null) return 0;

        else return series.size();
    }
}
