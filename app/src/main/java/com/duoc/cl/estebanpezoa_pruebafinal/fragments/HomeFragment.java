package com.duoc.cl.estebanpezoa_pruebafinal.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.duoc.cl.estebanpezoa_pruebafinal.ApiEndPointInterface;
import com.duoc.cl.estebanpezoa_pruebafinal.R;
import com.duoc.cl.estebanpezoa_pruebafinal.adapters.SeriesAdapter;
import com.duoc.cl.estebanpezoa_pruebafinal.modelos.Series;
import com.duoc.cl.estebanpezoa_pruebafinal.modelos.SeriesList;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class HomeFragment extends Fragment {

    RecyclerView rv;
    View v;
    SeriesAdapter adapter;
    private ProgressBar progressBar;

    private ArrayList<Series> series= new ArrayList<>();

    public HomeFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        this.v = view;
        progressBar = (ProgressBar) v.findViewById(R.id.progress_bar);
        Init();
        loadData();
    }

    private void Init() {
        rv = (RecyclerView) v.findViewById(R.id.rv_series);
        rv.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new SeriesAdapter(series);
        rv.setAdapter(adapter);
    }


    private void loadData() {
        try{progressBar.setVisibility(View.VISIBLE);
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.myjson.com/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        ApiEndPointInterface request = retrofit.create(ApiEndPointInterface.class);
        Call<SeriesList> call = request.getListAnime();

        call.enqueue(new Callback<SeriesList>() {
            @Override
            public void onResponse(Call<SeriesList> call, Response<SeriesList> response) {

                SeriesList jsonResponse = response.body();
                series = new ArrayList<>(jsonResponse.getList());
                adapter = new SeriesAdapter(series);
                rv.setAdapter(adapter);
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<SeriesList> call, Throwable t) {
                Log.d("Error",t.getMessage());
            }
        });}
        catch(Exception e) {
            e.getMessage().toString();

        }

    }
}
