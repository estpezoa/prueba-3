package com.duoc.cl.estebanpezoa_pruebafinal;

import com.duoc.cl.estebanpezoa_pruebafinal.modelos.Series;
import com.duoc.cl.estebanpezoa_pruebafinal.modelos.SeriesList;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

//import static com.duoc.cl.estebanpezoa_pruebafinal.Constants.APIConstants.ANIME_ID;
import static com.duoc.cl.estebanpezoa_pruebafinal.Constants.APIConstants.ANIME_LIST;

/**
 * Created by hardroidlabs on 18-05-17.
 */

public interface ApiEndPointInterface {
    @GET(ANIME_LIST)
    Call<SeriesList> getListAnime();
}
