package com.duoc.cl.estebanpezoa_pruebafinal;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class LoginActivity extends AppCompatActivity {

    EditText etNombre, etContrasena;
    Button btnIngresar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        etNombre = (EditText) findViewById(R.id.etNombre);
        etContrasena = (EditText) findViewById(R.id.etContrasena);
        btnIngresar = (Button) findViewById(R.id.btnIngresar);

        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Ingresar();
            }
        });
    }

    private void Ingresar() {
        if(etNombre.getText().toString().equalsIgnoreCase("admin") &&
                etContrasena.getText().toString().equals("admin"))
        {
            Intent i = new Intent(this, MainActivity.class);
            startActivity(i);
            finish();
        }
        else
        {
            etNombre.setError("Nombre y contraseña no coinciden");
        }
    }
}
